package com.seop.gamemanager.exception;

public class CNoMatchPassWardException extends RuntimeException {
    public CNoMatchPassWardException(String msg, Throwable t) {
        super(msg, t);
    }

    public CNoMatchPassWardException(String msg) {
        super(msg);
    }

    public CNoMatchPassWardException() {
        super();
    }
}
