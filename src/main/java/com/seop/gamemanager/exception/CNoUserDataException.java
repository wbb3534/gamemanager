package com.seop.gamemanager.exception;

public class CNoUserDataException extends RuntimeException {
    public CNoUserDataException(String msg, Throwable t) {
        super(msg, t);
    }

    public CNoUserDataException(String msg) {
        super(msg);
    }

    public CNoUserDataException() {
        super();
    }
}
