package com.seop.gamemanager.entity;

import com.seop.gamemanager.interfaces.CommonModelBuilder;
import com.seop.gamemanager.model.UserRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class GameUser {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false, length = 20, unique = true)
    private String userName;
    @Column(nullable = false, length = 20)
    private String passWard;
    @Column(nullable = false)
    private LocalDate birthday;
    @Column(nullable = false, length = 20)
    private String name;
    @Column(nullable = false, length = 13, unique = true)
    private String phone;
    @Column(columnDefinition = "TEXT")
    private String memo;

    private GameUser(GameUserBuilder builder) {
        this.userName = builder.userName;
        this.passWard = builder.passWard;
        this.birthday = builder.birthday;
        this.name = builder.name;
        this.phone = builder.phone;
    }
    public static class GameUserBuilder implements CommonModelBuilder<GameUser> {
        private final String userName;
        private final String passWard;
        private final LocalDate birthday;
        private final String name;
        private final String phone;

        public GameUserBuilder(UserRequest request) {
            this.userName = request.getUserName();
            this.passWard = request.getPassWard();
            this.birthday = request.getBirthday();
            this.name = request.getName();
            this.phone = request.getPhone();
        }
        @Override
        public GameUser build() {
            return new GameUser(this);
        }
    }
}
