package com.seop.gamemanager.model;

import com.seop.gamemanager.entity.GameUser;
import com.seop.gamemanager.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;


@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class LoginResponse {
    private Long id;
    private String userName;
    private String phone;

    private LoginResponse(LoginResponseBuilder builder) {
        this.id = builder.id;
        this.userName = builder.userName;
        this.phone = builder.phone;
    }

    public static class LoginResponseBuilder implements CommonModelBuilder<LoginResponse> {
        private final Long id;
        private final String userName;
        private final String phone;

        public LoginResponseBuilder(GameUser response) {
            this.id = response.getId();
            this.userName = response.getUserName();
            this.phone = response.getPhone();
        }
        @Override
        public LoginResponse build() {
            return new LoginResponse(this);
        }
    }
}
