package com.seop.gamemanager.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class UserRequest {
    @NotNull
    @Length(min = 5, max = 20)
    private String userName;
    @NotNull
    @Length(min = 8, max = 20)
    private String passWard;
    @NotNull
    private LocalDate birthday;
    @NotNull
    @Length(min = 2, max = 20)
    private String name;
    @NotNull
    @Length(min = 13, max = 13)
    private String phone;
}
