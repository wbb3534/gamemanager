package com.seop.gamemanager.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class LoginRequest {
    @NotNull
    @Length(min = 5, max = 20)
    private String userName;
    @NotNull
    @Length(min = 8, max = 20)
    private String passWard;
}
