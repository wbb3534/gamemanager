package com.seop.gamemanager.controller;

import com.seop.gamemanager.model.*;
import com.seop.gamemanager.service.GameUserService;
import com.seop.gamemanager.service.ResponseService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/game-user")
public class GameUserController {
    private final GameUserService gameUserService;

    @PostMapping("/join")
    public CommonResult setUser(@RequestBody @Valid UserRequest request) {
        gameUserService.setUser(request);

        return ResponseService.getSuccessResult();
    }

    @PostMapping("/login")
    public SingleResult<LoginResponse> doLogin(@RequestBody @Valid LoginRequest request) {
        return ResponseService.getSingleResult(gameUserService.doLogin(request));
    }

}
