package com.seop.gamemanager.service;

import com.seop.gamemanager.entity.GameUser;
import com.seop.gamemanager.exception.CDuplicateUserNameException;
import com.seop.gamemanager.exception.CNoMatchPassWardException;
import com.seop.gamemanager.exception.CNoUserDataException;
import com.seop.gamemanager.model.LoginRequest;
import com.seop.gamemanager.model.LoginResponse;
import com.seop.gamemanager.model.UserRequest;
import com.seop.gamemanager.repository.GameUserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class GameUserService {
    private final GameUserRepository gameUserRepository;

    public void setUser(UserRequest request) {
        long duplicateCount = gameUserRepository.countByUserName(request.getUserName()); //이름이 있으면 1 없으면 0개

        if (duplicateCount == 1) throw new CDuplicateUserNameException();

        GameUser addData = new GameUser.GameUserBuilder(request).build();

        gameUserRepository.save(addData);
    }
    //get역할이지만 개인정보가 들어있기 때문에 body로 무조건 받아야함
    public LoginResponse doLogin(LoginRequest request) {
        // Optional<GameUser> 레포지토리에 한 그대로 가져와야함
        Optional<GameUser> originData = gameUserRepository.findByUserName(request.getUserName());

        // 만약 오리진데이터가 비어있으면 CNoUserDataException()로던져라
        if (originData.isEmpty()) throw new CNoUserDataException();
        // 만약 비밀번호가 사람이 입력한 비밀번호가 일치하지 않으면 익셉션으로 버린다
        if (!originData.get().getPassWard().equals(request.getPassWard())) throw new CNoMatchPassWardException();

        return new LoginResponse.LoginResponseBuilder(originData.get()).build();

    }
}
