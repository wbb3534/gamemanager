package com.seop.gamemanager.repository;

import com.seop.gamemanager.entity.GameUser;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface GameUserRepository extends JpaRepository<GameUser, Long> {
    long countByUserName(String userName); //일치하는 아이디가 몇개인지 가져와라는 뜻

    Optional<GameUser> findByUserName(String userName);//선택사항 있어도되고 없어도되고 없어도 오류가 안난다.
}
