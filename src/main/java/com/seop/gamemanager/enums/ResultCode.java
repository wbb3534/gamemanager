package com.seop.gamemanager.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ResultCode {
    SUCCESS(0, "성공하였습니다.")
    , FAILED(-1, "실패하였습니다.")

    , MISSING_DATA(-10000, "데이터를 찾을 수 없습니다.")
    , WRONG_PHONE_NUMBER(-10001, "잘못된 핸드폰 번호입니다.")

    , DUPLICATE_USER_NAME(-20000, "중복된 아이디가 있습니다.")
    , NO_USER_DATA(-20001, "일치하는 아이디가 없습니다.")
    , NO_MATCH_PASS_WARD(-20001, "비밀번호가 일치하지 않습니다")

    ;

    private final Integer code;
    private final String msg;
}
