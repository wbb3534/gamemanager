package com.seop.gamemanager.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}
